import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @org.junit.Test
    public void TestStringSum0Numbers() {
        assertTrue(Main.stringSum("")==0);
    }
    @org.junit.Test
    public void TestStringSum1Number() {
        assertTrue(Main.stringSum("1")==1);
    }
    @org.junit.Test
    public void TestStringSum2Numbers() {
        assertTrue(Main.stringSum("1,2")==3);
    }
    @org.junit.Test
    public void TestStringSum5Numbers() {
        assertTrue(Main.stringSum("1,2,3,4,5")==15);
    }
    @org.junit.Test
    public void TestStringSumNewLine() {
        assertTrue(Main.stringSum("1\n2,3")==6);
    }
    @org.junit.Test
    public void TestStringSumDelimiters() {
        assertTrue(Main.stringSum("//;\n1;2")==3);
    }
    @org.junit.Test(expected = Main.StringSumNegativeException.class)
    public void TestStringSumNegatives() {
        Main.stringSum("2,-5");
    }
}