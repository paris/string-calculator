import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world!");

        System.out.println(stringSum(args[0]));
    }

    public static int stringSum(String nums){
        String tmp = "";
        ArrayList<String> numbers = new ArrayList<String>();
        numbers.add(0,"0");
        //int[] numbers;
        int marker = 0;
        for (int i = 0; i < nums.length(); i++){
            char c = nums.charAt(i);
            String str = "" + c;

            if (str.equals("-")){
                if (i+1 < nums.length()){
                    if (Character.isDigit(nums.charAt(i+1))) {
                        throw new StringSumNegativeException("Negatives are not allowed: -" + nums.charAt(i + 1));
                        //System.out.println("PRETEND THIS IS AN EXCEPTION! negatives not allowed: -" + nums.charAt(i + 1));
                    }
                }
            }
            if (Character.isDigit(c)){
                String result = "" + numbers.get(marker) + c;
                numbers.set(marker, result.toString());
            }
            else {
                marker ++;
                numbers.add(marker,"0");
            }
        }
        int sum = 0;
        for(String d : numbers)
            //System.out.println(d);
            sum += Integer.parseInt(d);
        return sum;
    }

    public static class StringSumNegativeException extends RuntimeException{
        public StringSumNegativeException(String message) {
            super(message);
        }
    }
}
